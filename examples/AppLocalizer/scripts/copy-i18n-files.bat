@chcp 1251
echo off


set PO_UTILITY=scripts\tools\poFileUtility.EXE

echo.
echo ��������� �������
echo.

set PROJNAME=AppLocalizer
set BUILD=Release
set LANGDIR=bin\lang


cd ..

echo.
echo �������� ������ �������� win32
echo.
del /f /q %LANGDIR%\*win32-*.po?

echo.
echo ����������� win64 ������ �������� � ����� ��� ���� ����������
echo.
copy %LANGDIR%\%PROJNAME%-win64-%BUILD%.*.po  %LANGDIR%\%PROJNAME%.*.po

echo.
echo ����������� win64 ������� ����������� �������� � ����� ������
echo.
copy %LANGDIR%\%PROJNAME%-win64-%BUILD%.pot   %LANGDIR%\%PROJNAME%.pot

echo.
echo ������� ����� � ����� �������� ��� ����� ��������� � ���������� � .en.po
echo.
%PO_UTILITY% %LANGDIR%\%PROJNAME%.pot %LANGDIR%\%PROJNAME%.en.po transfer
